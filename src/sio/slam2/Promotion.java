package sio.slam2;

import java.util.ArrayList;

public class Promotion {
	private String niveau;//sio1 ou sio2
	private ArrayList<Etudiant> lesEtudiants;

	public ArrayList<Etudiant> getLesEtudiants() {
		return lesEtudiants;
	}

	public Promotion(String niveau) {
		this.niveau = niveau;
		this.lesEtudiants=new ArrayList<Etudiant>();
	}
	
	public String getNiveau() {
		return niveau;
	}

	public void ajouteEtudiant(Etudiant e){
		this.lesEtudiants.add(e);
	}

	//------------------------------------------------------------------------
	//METHODES A COMPLETER----------------------------------------------------
	//------------------------------------------------------------------------
	public double moyenne(){
		double moy = 0.0;

		ArrayList<Etudiant> lesEtuPres = etudiantsPresents();

		for (int i=0; i<lesEtuPres.size();i++){
				moy = moy + lesEtuPres.get(i).getNoteJava();
		}
		if (lesEtuPres.size()!=0) {
			moy = moy / lesEtuPres.size();
		}
		return moy;

	}
	//------------------------------------------------------------------------	
	public double noteMin(){

		ArrayList<Etudiant> lesEtuPres = etudiantsPresents();

		double noteM = lesEtuPres.get(0).getNoteJava();

		for (int i=1; i<lesEtuPres.size();i++) {
			if (lesEtuPres.get(i).getNoteJava() < noteM){
				noteM = lesEtuPres.get(i).getNoteJava();
			}
		}
		return noteM;
	}
	//------------------------------------------------------------------------

	/**
	 * Retourne la note max des étudiants présents
	 * @return
	 */
	public double noteMax(){
		double max = 0;
		for(int i=0;i<this.etudiantsPresents().size();i++){
			if(this.etudiantsPresents().get(i).getNoteJava() > max){
				max=this.etudiantsPresents().get(i).getNoteJava();
			}
		}
		return max;
	}

	//------------------------------------------------------------------------
	public Etudiant rechercheEtudiant(String prenom){
		Etudiant etuRech = null;

		boolean trouve = false;
		int i=0;

		while (i<lesEtudiants.size() & !trouve){
			if (lesEtudiants.get(i).getPrenom().equals(prenom)){
				trouve = true;
				etuRech = lesEtudiants.get(i);
			}
			else{
				i++;
			}
		}

		return etuRech;
	}

	//------------------------------------------------------------------------

	/**
	 * Renvoie l'étudiant ayant la note maximale (premier étudiant rencontré)
	 * @return
	 */
	public Etudiant etudiantMax(){
		Etudiant max = this.etudiantsPresents().get(0);
		for(int i=0;i<this.etudiantsPresents().size();i++){
			if(this.etudiantsPresents().get(i).getNoteJava() > max.getNoteJava())
				max = this.etudiantsPresents().get(i);
		}
		return max;

	}

	//------------------------------------------------------------------------
	public ArrayList<Etudiant> etudiantsPresents(){
		ArrayList<Etudiant> lesEtuPresents= new ArrayList<Etudiant>();

		for (int i=0; i<this.lesEtudiants.size();i++){
			if (this.lesEtudiants.get(i).getNoteJava() != -1){
				lesEtuPresents.add(this.lesEtudiants.get(i));
			}
		}

		return lesEtuPresents;
	}

	//------------------------------------------------------------------------

	/**
	 * Retourne la liste des étudiants ayant eu la note max
	 * @return
	 */
	public ArrayList<Etudiant> etudiantsMax(){
		ArrayList<Etudiant> max = new ArrayList<Etudiant>();

		for(int i=0;i<this.etudiantsPresents().size();i++){

			if(this.etudiantsPresents().get(i).getNoteJava()==this.noteMax())

				max.add(this.etudiantsPresents().get(i));
		}

		return max;

	}
	//------------------------------------------------------------------------
	public ArrayList<Etudiant> etudiantsRecus(){
		ArrayList<Etudiant> etuRecu = new ArrayList<Etudiant>();

		double moy = this.moyenne();

		for(int i=0;i<this.etudiantsPresents().size();i++) {

			if (this.etudiantsPresents().get(i).getNoteJava() > moy){
				etuRecu.add(this.etudiantsPresents().get(i));
			}
		}

		return etuRecu;
	}


}
