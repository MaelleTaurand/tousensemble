package sio.slam2;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Promotion sio1=new Promotion("sio1");

        Etudiant e;
        e=new Etudiant("Tom", 19);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("Tim", 5);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("Tam", 15);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("John", -1);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("Léna", 19);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("Stan", 5);
        sio1.ajouteEtudiant(e);
        e=new Etudiant("Simon", 11);
        sio1.ajouteEtudiant(e);


        ArrayList<Etudiant> liste = new ArrayList<Etudiant>();

        // test moyenne
        assert sio1.moyenne() == (double)(19+5+15+19+5+11)/6 :"Pb moyenne";
        //assert sio1.moyenne() == 0 :"Pb moyenne si aucun présent";

        //test noteMin
        assert sio1.noteMin() == 5 : "pb note Min";

        //test noteMax
        assert sio1.noteMax() == 19:"Pb noteMax";


        //test etudiantsPresents
        assert sio1.etudiantsPresents().size() == 6 :"Pb etudiants présents";

        // rechercheEtu
        assert sio1.rechercheEtudiant("Léna").getPrenom().equals("Léna"):"Pb recherche";
        assert sio1.rechercheEtudiant("Johnny") == null : "Pb recherche Etu non présent";

        // test etudiantMax
        assert sio1.etudiantMax().getPrenom().equals("Tom") : "Pb etudiant max";

        //test etudiantsMax
        assert sio1.etudiantsMax().size() == 2;
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(0)) == true: " Problème étudiant ayant eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(1)) == false:" Problème étudiant n'ayant pas eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(2)) == false:" Problème étudiant n'ayant pas eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(3)) == false:" Problème étudiant n'ayant pas eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(4)) == true:" Problème étudiant ayant eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(5)) == false:" Problème étudiant n'ayant pas eu note max";
        assert sio1.etudiantsMax().contains(sio1.getLesEtudiants().get(6)) == false:" Problème étudiant n'ayant pas eu note max";


        //etudiantsRecus

        assert sio1.etudiantsRecus().size() == 3;
        assert sio1.etudiantsRecus().contains(sio1.getLesEtudiants().get(0)): "Probleme etu reçu";
        assert !sio1.etudiantsRecus().contains(sio1.getLesEtudiants().get(1)): "Probleme etu non reçu";


    }
}
