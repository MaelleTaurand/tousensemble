package sio.slam2;

public class Etudiant{
	private String prenom;
	private double noteJava;//-1 si absent
	
	public Etudiant(String prenom, double noteJava) {
		this.prenom = prenom;
		this.noteJava = noteJava;
	}

	public String getPrenom() {
		return prenom;
	}

	public double getNoteJava() {
		return noteJava;
	}
}